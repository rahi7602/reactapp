import React from "react";
import classes from "../../assets/global.scss";
import PropTypes from "prop-types";
import { Route, Link } from "react-router-dom";
import styled from "styled-components";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import FormLabel from "@material-ui/core/FormLabel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import Paper from "@material-ui/core/Paper";
import { withStyles } from '@material-ui/core/styles';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  Chip,
  Typography,
  DialogActions
} from "@material-ui/core";
import { Add, HelpOutline } from "@material-ui/icons";
import { FaQuestion } from "react-icons/fa";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import * as faker from "faker";

import TextField from "@material-ui/core/TextField";
import clsx from "clsx";
import styles from "./users.scss";
import Input from "@material-ui/core/Input";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Button from "@material-ui/core/Button";
import Tooltip from "@material-ui/core/Tooltip";
import MenuItem from "@material-ui/core/MenuItem";
import userIcon from "./../../assets/imgs/main/addUser.png";
import mngUser from "./../../assets/imgs/main/manage.png";
import addUserWht from "./../../assets/imgs/main/addUserWht.png";

function createData(username, role, email, status) {
  return { username, role, email, status };
}

const rows = [];

for (let i = 0; i <= 40; i++) {
  let row = createData(
    faker.name.findName(),
    faker.random.arrayElement(["Administrator", "Student"]),
    faker.internet.email(),
    faker.random.arrayElement(["Active", "Inactive"])
  );
  rows.push(row);
};

const stylesMaterial = {
  tooltip: {
    color: "white",
    backgroundColor: "#D95D5D"
  }
};

const currencies = [
  {
    value: "student",
    label: "Student"
  },
  {
    value: "teacher",
    label: "Teacher"
  },
  {
    value: "Parents",
    label: "parents"
  }
];
export class Users extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleRole = this.handleRole.bind(this);
    this.state = {
      'open': false,
      'name': "",
      'role':currencies[0]
    };
  }

  handleClose() {
    this.setState({ open: false });
  }

  handleShow() {
    this.setState({ open: true });
  }

  handleRole = (e) => {
      console.log("...role",e);
     this.setState({'role': e.target });
  };

  submitForm = () => {
    alert("form_submit");
  };

  renderModal() {
   
    return (
      <div>
        <Dialog
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
          open={this.state.open}
          fullWidth={true}
          scroll={"body"}
          maxWidth={"sm"}
          className={styles.DialogContainer}
        >
          <DialogTitle id="form-dialog-title" className={[classes.DialogHeading,styles.userDialog].join(' ')}>
            <img
              src={userIcon}
              style={{ width: "35px", verticalAlign: "unset",paddingRight:'15px' }}
            />
            Add New Question
          </DialogTitle>
          <DialogContent className={classes.DialogWrap}>
            <form className={[styles.FromWrap,styles.userWrap].join(' ')}>
              <FormControl fullWidth className={classes.margin}>
                <InputLabel htmlFor="adornment-amount" className={styles.label}>
                  User Role
                </InputLabel>
                <TextField
                  id="filled-select-currency"
                  select
                  label="Select"
                  className={[classes.textField, styles.inputSelect].join(' ')}
                 
                  value={this.state.role.value}
                  onChange={this.handleRole}
                  SelectProps={{
                    MenuProps: {
                      className: classes.menu
                    }
                  }}
                  margin="normal"
                  variant="outlined"
                >
                  {currencies.map(option => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </FormControl>
              <FormControl
                fullWidth
                className={[classes.margin, styles.FormGroup2].join(" ")}
              >
                <InputLabel htmlFor="adornment-amount" className={styles.label}>
                  Name
                </InputLabel>
                <TextField
                  id="outlined-bare2"
                  className={styles.textField}
                  margin="normal"
                
                  variant="outlined"
                  inputProps={{ "aria-label": "bare" }}
                  name="txt1"
                  defaultValue=""
                  placeholder="Text for option 1"
                />
              </FormControl>
              <FormControl
                fullWidth
                className={[classes.margin, styles.FormGroup2].join(" ")}
              >
                <InputLabel htmlFor="adornment-amount" className={styles.label}>
                  E-mail
                </InputLabel>
                <TextField
                  id="outlined-bare3"
                  className={styles.textField}
                  defaultValue=" "
                  margin="normal"
                 
                  variant="outlined"
                  inputProps={{ "aria-label": "bare" }}
                  name="txt2"
                  defaultValue=""
                  placeholder="Text for option 2"
                />
              </FormControl>
              <FormControl
                fullWidth
                className={[classes.margin, styles.FormGroup2].join(" ")}
              >
                <InputLabel htmlFor="adornment-amount" className={styles.label}>
                  Password
                </InputLabel>
              
                <TextField
                  id="outlined-bare4"
                  className={styles.textField}
                  defaultValue=" "
                
                  margin="normal"
                  variant="outlined"
                  inputProps={{ "aria-label": "bare" }}
                  name="password"
                  defaultValue=""
                  placeholder="********"
                  type="password"
                  ref="password"
                />
               
              </FormControl>
              
              <FormControl
                fullWidth
                className={[classes.margin, styles.FormGroup2].join(" ")}
              >
                <InputLabel htmlFor="adornment-amount" className={styles.label}>
                  Password Again
                </InputLabel>
                <Tooltip open={true} classes={this.props.classes} title="Sorry,password don't match" placement="top">
                <TextField
                  id="outlined-bare5"
                  className={styles.textField}
                  defaultValue=" "
               
                  margin="normal"
                  variant="outlined"
                  inputProps={{ "aria-label": "bare" }}
                  name="passwordAgain"
                  defaultValue=""
                  placeholder="********"
                  type="password"
                />
                </Tooltip>
              </FormControl>

              <Grid item xs={12}>
                <Button
                  variant="contained"
                  type="submit"
                  onClick={this.submitForm}
                  component="span"
                  className={[classes.margin, styles.submitBtn].join(" ")}
                >
                  CREATE
                </Button>
              </Grid>
            </form>
          </DialogContent>
        </Dialog>
      </div>
    );
  }

  render() {
    return (
      <div className={classes.root}>
        {this.renderModal()}
        <Grid container spacing={3}>
          <Grid item xs={6}>
          <Typography
              className={[classes.headingColor, classes.pageTitle].join(' ')}
              variant="h5"
              gutterBottom
            >
              <img src={mngUser} className={classes.imgStyle} />
              Manage Questions
            </Typography>
          </Grid>
          <Grid style={{ textAlign: "right" }} item xs={6}>
            <Button
              onClick={this.handleShow}
              className={classes.btnPrimary}
              variant="contained"
            >
              <img src={addUserWht} style={{width:'35px',marginRight:'10px'}} />
              ADD USER
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          <Grid item xs={12}>
          <h2 className={classes.pageHeading}>All Users</h2>
          </Grid>
          <Grid item xs={12}>
            <Paper>
              <Table>
                <TableHead className={classes.TableHead}>
                  <TableRow>
                    <TableCell
                      style={{ width: "20%" }}
                      className={classes.theadercell}
                    >
                      User’s Name
                    </TableCell>
                    <TableCell className={classes.theadercell} align="left">
                      Role
                    </TableCell>
                    <TableCell className={classes.theadercell} align="left">
                      Email
                    </TableCell>
                    <TableCell className={classes.theadercell} align="left">
                      Status
                    </TableCell>
                    <TableCell className={classes.theadercell} align="right">
                      &nbsp;
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {rows.map((row, index) => (
                    <TableRow
                      className={
                        index % 2 === 0 ? classes.evenrow : classes.oddrow
                      }
                      key={row.username}
                    >
                      <TableCell
                        className={classes.headingColor}
                        component="th"
                        scope="row"
                      >
                        {row.username}
                      </TableCell>
                      <TableCell align="left">{row.role}</TableCell>
                      <TableCell align="left">{row.email}</TableCell>
                      <TableCell align="left">{row.status}</TableCell>

                      <TableCell align="right">
                      <Button
                          className={[classes.btnPrimary, classes.delBtn].join(' ')}
                          variant="contained"
                        >
                          DELETE
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const userApp = withStyles(stylesMaterial)(Users);
export {userApp as UsersPage}
