import React, { Component } from "react";
import Table from 'react-bootstrap/Table';
import PropTypes from "prop-types";
import { Route, Link } from "react-router-dom";
import styled from "styled-components";
import classes from './../main/dashboard/dashboard.scss';


export class ManageUsers extends Component {
  render() {
    return (
        <div className={classes.main}>
            <Table striped bordered hover>
  <thead>
    <tr>
      <th>#</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Username</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
      <td>2</td>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <td>3</td>
      <td colSpan="2">Larry the Bird</td>
      <td>@twitter</td>
    </tr>
  </tbody>
</Table>
        </div>
    )
  }
}
