import React from 'react'
import classes from './demo.scss'
import PropTypes from 'prop-types'
import {Route, Link} from 'react-router-dom'
import styled from 'styled-components'

export class DemoPage extends React.Component{
    render(){
        return(
            <div className={classes.main}>
                <div className={classes.header}>
                    <Link to={"/"}><img className={classes.img_size} src={require("../../assets/imgs/dashboard/professor_mark.png")}/></Link>
                    <ul className={classes.ul_style}>
                        <li><a>WHAT DOES THIS DO?</a></li>
                        <li><a>WHAT IS IT FOR?</a></li>
                        <li><a>HOW MUCH DOES IT COST?</a></li>
                        <li><a>WHO CAN I TALK TO?</a></li>
                        <li><Link to={"/login"} className={classes.signin}>Sign In</Link></li>
                    </ul>
                </div>
                <div className={classes.content}>
                    <div className={classes.content_1_left}>
                        <img className={classes.mark_img} src={require('../../assets/imgs/dashboard/up_arrow.png')}></img>
                        <p className={classes.red_text}>
                            Level-up assessments
                            and get your students
                            learning what you intend!
                        </p>
                        <p className={classes.common_text}>
                        Using Intelianza, instructors can better achieve learning outcomes by delivering a more advanced system of assessments. Break away from the old-fashioned cookie-cutter approach, and begin helping your students understand and retain what they should.
                        </p>
                        <p>
                            <a className={classes.learn_more}>
                                LEARN MORE
                            </a>

                        </p>
                    </div>
                    <img className={classes.img_1_right} 
                    src={require("../../assets/imgs/dashboard/mark_img_1.png")}/>
                </div>

                <div className={classes.content1}>
                    <div className={classes.item}>
                        <img className={classes.img_1_right} 
                            src={require("../../assets/imgs/dashboard/demo_img_2.png")}/>
                        <div className={classes.content_1_left}>
                            <img className={classes.mark_img} src={require('../../assets/imgs/dashboard/sheld.png')}></img>
                            <p className={classes.red_text}>
                            Stay compliant with our
                            secure data solutions.
                            </p>
                            <p className={classes.common_text}>
                                Because Intelianza can store your students’ data in their country of origin, you’re able to comply to data sovereignty, and data localization laws with a turn-key solution.
                            </p>
                           
                        </div>
                    </div>
                    <div className={classes.item}>
                        
                        <div className={classes.content_1_left}>
                            <img className={classes.mark_img} src={require('../../assets/imgs/dashboard/sheld.png')}></img>
                            <p className={classes.red_text}>
                            Get all your reports in
                            a single location!
                            </p>
                            <p className={classes.common_text}>
                                Because Intelianza can store your students’ data in their country of origin, you’re able to comply to data sovereignty, and data localization laws with a turn-key solution.
                            </p>
                        </div>
                        <img className={classes.img_1_right} 
                            src={require("../../assets/imgs/dashboard/demo_img_3.png")}/>
                    </div>
                    </div>

                    <div className={classes.middle_footer}>
                        <img className={classes.bubble_img} src={require('../../assets/imgs/dashboard/middle_footer.png')}/> 
                        <div className={classes.middle_footer_content}>
                            <img className={classes.mark_img} src={require('../../assets/imgs/dashboard/video_img.png')}></img>
                            <p className={classes.red_text}>
                            SEE A DEMO
                            </p>
                            <p className={classes.common_text}>
                                See what Intelianza can do to improve the way your instructors help their students learn and better achieve desired learning outcomes. Let’s find a time that works for you!
                            </p>

                           <a className={classes.learn_more}>SCHEDULE NOW</a>
                        
                        </div>
                    </div>
                
                
                <div className={classes.footer}>
                    <img className={classes.footer_img} src={require('../../assets/imgs/dashboard/footer.png')}/>
                    <ul className={classes.short_links}>
                        <li><a>WHAT DOES THIS DO?</a></li>
                        <li><a>WHAT IS IT FOR?</a></li>
                        <li><a>HOW MUCH DOES IT COST?</a></li>
                        <li><a>WHO CAN I TALK TO?</a></li>
                    </ul>

                </div>

            </div>
        )
    }


}