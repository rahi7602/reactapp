import React from "react";
import classes from "../../assets/global.scss";
import PropTypes from "prop-types";
import { Route, Link } from "react-router-dom";
import styled from "styled-components";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import FormLabel from "@material-ui/core/FormLabel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import Paper from "@material-ui/core/Paper";
import {
  Dialog,
  Button,
  DialogTitle,
  DialogContent,
  Chip,
  Typography,
  DialogActions
} from "@material-ui/core";
import { Add, HelpOutline } from "@material-ui/icons";
import { FaQuestion } from "react-icons/fa";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import * as faker from "faker";

import TextField from "@material-ui/core/TextField";
import clsx from "clsx";
import styles from "./roles.scss";
import Input from "@material-ui/core/Input";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Tooltip from "@material-ui/core/Tooltip";
import MenuItem from "@material-ui/core/MenuItem";
import roleImg from './../../assets/imgs/main/role.png';

import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import addUserWht from "./../../assets/imgs/main/addUserWht.png";
import roleBlue from "./../../assets/imgs/main/roleBlue.png";
function createData(role, numbetofusers) {
  return { role, numbetofusers };
}

const rows = [];

for (let i = 0; i <= 4; i++) {
  let row = createData(
    faker.random.arrayElement(["Administrator", "Manager", "Student"]),
    faker.random.number()
  );
  rows.push(row);
}



const currencies = [
    {
      value: "student",
      label: "Student"
    },
    {
      value: "teacher",
      label: "Teacher"
    },
    {
      value: "Parents",
      label: "parents"
    }
  ];

  const labelUsers = [
    {
      value: "create users",
      label: "create users"
    },
    {
      value: "view users",
      label: "view users"
    },
    {
      value: "modify users",
      label: "modify users"
    },
    {
        value: "delete users",
        label: "delete users"
      },
      {
        value: "tag users",
        label: "tag users"
      }
  ];

  const labelRole = [
    {
      value: "create roles",
      label: "create roles"
    },
    {
      value: "view roles",
      label: "view roles"
    },
    {
      value: "modify roles",
      label: "modify roles"
    },
    {
        value: "delete roles",
        label: "delete roles"
      },
      {
        value: "share roles",
        label: "share roles"
      }
  ];


export class RolesPage extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.state = {
      open: false,
      'role':currencies[0]
    };
  }

  handleClose() {
    this.setState({ open: false });
  }

  handleShow() {
    this.setState({ open: true });
  }

  handleRole = (e) => {
    console.log("...role",e);
   this.setState({'role': e.target });
};

submitForm = () => {
  alert("form_submit");
};





  renderModal() {
    return (
      <div>
        <Dialog
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
          open={this.state.open}
          fullWidth={true}
          scroll={"body"}
          maxWidth={"sm"}
          className={[styles.DialogContainer, styles.rolesDialog].join(" ")}
        >
          <DialogTitle
            id="form-dialog-title"
            className={[classes.DialogHeading, styles.userDialog].join(" ")}
          >
            <img
              src={roleImg}
              style={{
                width: "35px",
                verticalAlign: "bottom",
                marginRight: "15px"
              }}
            />
            Add New Role
          </DialogTitle>
          <DialogContent dividers>
            <form className={[styles.FromWrap, styles.RoleWrap].join(" ")}>
              <Grid item xs={6}>
                <FormControl
                  fullWidth
                  className={[classes.margin, styles.FormGroup2].join(" ")}
                >
                  <InputLabel
                    htmlFor="adornment-amount"
                    className={styles.label}
                  >
                    Role Name
                  </InputLabel>
                  <TextField
                    id="outlined-bare2"
                    className={styles.textField}
                    margin="normal"
                    variant="outlined"
                    inputProps={{ "aria-label": "bare" }}
                    name="txt1"
                    defaultValue=""
                    placeholder="New Role Name Here"
                  />
                </FormControl>

                <FormControl
                  fullWidth
                  className={[classes.margin, styles.FormGroup2].join(" ")}
                >
                  <InputLabel
                    htmlFor="adornment-amount"
                    className={styles.label}
                  >
                    Parent Role
                  </InputLabel>
                  <TextField
                    id="filled-select-currency"
                    select
                    label="Select"
                    className={[classes.textField, styles.select3].join(
                      " "
                    )}
                    value={this.state.role.value}
                    onChange={this.handleRole}
                    SelectProps={{
                      MenuProps: {
                        className: classes.menu
                      }
                    }}
                    margin="normal"
                    variant="outlined"
                  >
                    {currencies.map(option => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </FormControl>
                <Typography
                  variant="h6"
                  component="h6"
                  className={styles.labrlText}
                >
                  Role Permissions
                </Typography>
                <Typography
                  variant="h6"
                  component="b"
                  className={styles.normalText}
                >
                  Allow users with this role to:
                </Typography>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography
                    variant="h6"
                    component="h6"
                    className={styles.normalTextinner}
                  >
                    Users
                  </Typography>

                    {/* USERS */}
                  {labelUsers.map((item,index) => {
                      console.log(item);
                  return  (<FormControlLabel
                    control={
                      <Checkbox
                        // checked={}
                        // onChange={}
                        value={item.value}
                        color="primary"
                      />
                    }
                    className={styles.checkLabel}
                    label={item.label}
                    key={index}
                  />)

                  })}

                 {/* ROLES */}
                 <Typography
                    variant="h6"
                    component="h6"
                    className={styles.normalTextinner}
                  >
                    Roles
                  </Typography>

                 {labelRole.map((item,index) => {
                      console.log(item);
                  return  (<FormControlLabel
                    control={
                      <Checkbox
                        // checked={}
                        // onChange={}
                        value={item.value}
                        color="primary"
                      />
                    }
                    className={styles.checkLabel}
                    label={item.label}
                    key={index}
                  />)

                  })}


                  
                </Grid>
                <Grid item xs>
                <Typography
                    variant="h6"
                    component="h6"
                    className={styles.normalTextinner}
                  >
                    Tags
                  </Typography>

                    {/* TAGS */}
                  {labelUsers.map((item,index) => {
                      console.log(item);
                  return  (<FormControlLabel
                    control={
                      <Checkbox
                        // checked={}
                        // onChange={}
                        value={item.value}
                        color="primary"
                      />
                    }
                    className={styles.checkLabel}
                    label={item.label}
                    key={index}
                  />)

                  })}

                 {/* Questions  */}
                 <Typography
                    variant="h6"
                    component="h6"
                    className={styles.normalTextinner}
                  >
                    Questions 
                  </Typography>

                 {labelRole.map((item,index) => {
                      console.log(item);
                  return  (<FormControlLabel
                    control={
                      <Checkbox
                        // checked={}
                        // onChange={}
                        value={item.value}
                        color="primary"
                      />
                    }
                    className={styles.checkLabel}
                    label={item.label}
                    key={index}
                  />)

                  })}

                </Grid>
                <Grid item xs>
                <Typography
                    variant="h6"
                    component="h6"
                    className={styles.normalTextinner}
                  >
                    Question Sets
                  </Typography>

                    {/* TAGS */}
                  {labelUsers.map((item,index) => {
                      console.log(item);
                  return  (<FormControlLabel
                    control={
                      <Checkbox
                        // checked={}
                        // onChange={}
                        value={item.value}
                        color="primary"
                      />
                    }
                    className={styles.checkLabel}
                    label={item.label}
                    key={index}
                  />)

                  })}

          
                </Grid>
              </Grid>
              <Button
                  variant="contained"
                  type="submit"
                  onClick={this.submitForm}
                  component="span"
                  className={[classes.margin, styles.submitBtn, styles.roleBtn].join(" ")}
                  style={{marginTop:'20px'}}
                >
                  CREATE
                </Button>
            </form>
          </DialogContent>
        </Dialog>
      </div>
    );
  }

  render() {
    return (
      <div className={classes.root}>
        {this.renderModal()}
        <Grid container spacing={3}>
          <Grid item xs={6}>
          <Typography
              className={[classes.headingColor, classes.pageTitle].join(' ')}
              variant="h5"
              gutterBottom
            >
              <img src={roleBlue} className={classes.imgStyle} />
              Manage Roles
            </Typography>
          </Grid>

          <Grid style={{ textAlign: "right" }} item xs={6}>
            <Button
              onClick={this.handleShow}
              className={classes.btnPrimary}
              variant="contained"
            >
               <img src={addUserWht} style={{width:'35px',marginRight:'10px'}} />
              ADD ROLE
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          <Grid item xs={12}>
          <h2 className={classes.pageHeading}>All Roles</h2>
          </Grid>
          <Grid item xs={12}>
            <Paper>
              <Table>
                <TableHead className={classes.TableHead}>
                  <TableRow>
                    <TableCell
                      style={{ width: "20%" }}
                      className={classes.theadercell}
                    >
                      Role
                    </TableCell>
                    <TableCell className={classes.theadercell} align="left">
                      Number of Users With Role
                    </TableCell>
                    <TableCell className={classes.theadercell} align="right">
                      &nbsp;
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {rows.map((row, index) => (
                    <TableRow
                      className={
                        index % 2 === 0 ? classes.evenrow : classes.oddrow
                      }
                      key={index}
                    >
                      <TableCell
                        className={classes.headingColor}
                        component="th"
                        scope="row"
                      >
                        {row.role}
                      </TableCell>
                      <TableCell align="left">{row.numbetofusers}</TableCell>

                      <TableCell align="right">
                      <Button
                          className={[classes.btnPrimary, classes.delBtn].join(' ')}
                          variant="contained"
                        >
                          DELETE
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}
