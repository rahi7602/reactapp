import React from "react";
import classes from "../../assets/global.scss";
import PropTypes from "prop-types";
import { Route, Link } from "react-router-dom";
import styled from "styled-components";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import FormLabel from "@material-ui/core/FormLabel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import quesStyle from "./questions.scss";
import Paper from "@material-ui/core/Paper";
import {
  Dialog,
  Button,
  DialogTitle,
  DialogContent,
  Chip,
  Typography,
  DialogActions
} from "@material-ui/core";
import { Add, HelpOutline } from "@material-ui/icons";
import { FaQuestion } from "react-icons/fa";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import * as faker from "faker";

import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import List from "@material-ui/core/List";
import { FormWizard } from "./FormWizard.jsx";
///IMAGE
import iconInbox from "./../../assets/imgs/main/icon-inbox.png";
import iconText from "./../../assets/imgs/main/iconText.png";
import iconMatch from "./../../assets/imgs/main/iconMatching.png";
import iconShort from "./../../assets/imgs/main/iconShort.png";
import question from "./../../assets/imgs/main/question.png";
import addquWht from "./../../assets/imgs/main/addquWht.png";

function createData(question, createddate, email, status, tags) {
  return { question, createddate, email, status, tags };
}

const rows = [];

for (let i = 0; i <= 40; i++) {
  let tags = faker.random.words().split(" ");
  let row = createData(
    faker.random.words(),
    "2015-01-01",
    faker.internet.email(),
    faker.random.arrayElement(["Draft", "Published", "Sent"]),
    tags
  );
  rows.push(row);
}
export class QuestionsPage extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.state = {
      open: false,
      formIdentity: "Multiple Choice",
      activeClass:'Multiple Choice'
    };
  }

  handleClose() {
    this.setState({ open: false });
  }

  handleShow() {
    this.setState({ open: true });
  }

  

  getContent = (formIdentity,e) => {
    this.setState({ formIdentity: formIdentity,activeClass:formIdentity });
   // console.log(e.target.className="addClasss");
  };

  renderModal() {
    const ModalIcon = [iconInbox, iconText, iconMatch, iconShort];
    return (
      <div>
        <Dialog
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
          open={this.state.open}
          fullWidth={true}
          scroll={"body"}
          maxWidth={"sm"}
          // contentStyle={{ width: "100%", maxWidth: "800px" }}
          className={quesStyle.DialogContainer}
        >
          <DialogTitle id="form-dialog-title" className={[classes.DialogHeading,quesStyle.quesHeading].join(' ')}>
            <Add style={{ verticalAlign: "sub" }} /> Add New Question
          </DialogTitle>
          <DialogContent className={classes.DialogWrap}>
            <Grid container spacing={3}>
              <Grid item xs={3} className={quesStyle.iconWrap}>
                <List>

                  { 
                    ["Multiple Choice", "Text", "Matching", "Sorting"].map(
                    (text, index) => (                     
                      
                      <ListItem
                        onClick={(e) => this.getContent(text,e)}
                        button
                        key={text}
                        className={ this.state.activeClass === text ? [quesStyle.iconInner, quesStyle.itemActive].join(' ') : quesStyle.iconInner}
                      >
                        {/* <ListItemIcon>
                          {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                        </ListItemIcon> */}
                        <img
                          className={quesStyle.iconModal}
                          src={ModalIcon[index]}
                        />{" "}
                        <ListItemText primary={text} />
                      </ListItem>
                    )
                  )}
                </List>
              </Grid>
              <Grid item xs={9} className={classes.formWrap}>
                <FormWizard formIdentity={this.state.formIdentity} />
              </Grid>
            </Grid>
          </DialogContent>
        </Dialog>
      </div>
    );
  }

  render() {
    return (
      <div className={classes.root}>
        {this.renderModal()}
        <Grid container spacing={3}>
          <Grid item xs={6}>
            <Typography
              className={[classes.headingColor, classes.pageTitle].join(' ')}
              variant="h5"
              gutterBottom
            >
              <img src={question} className={classes.imgStyle} />
              Manage Questions
            </Typography>
          </Grid>

          <Grid style={{ textAlign: "right" }} item xs={6}>
            <Button
              onClick={this.handleShow}
              className={classes.btnPrimary}
              variant="contained"
            >
               <img src={addquWht} style={{width:'30px',marginRight:'12px'}}/>
              ADD QUESTION
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <h2 className={classes.pageHeading}>All Questions</h2>
          </Grid>
          <Grid item xs={12}>
            <Paper>
              <Table>
                <TableHead className={classes.TableHead}>
                  <TableRow>
                    <TableCell
                      style={{ width: "20%" }}
                      className={classes.theadercell}
                    >
                      Question Text
                    </TableCell>
                    <TableCell
                      style={{ width: "15%" }}
                      className={classes.theadercell}
                      align="left"
                    >
                      Date Created
                    </TableCell>
                    <TableCell
                      style={{ width: "20%" }}
                      className={classes.theadercell}
                      align="left"
                    >
                      Created By
                    </TableCell>
                    <TableCell
                      style={{ width: "10%" }}
                      className={classes.theadercell}
                      align="left"
                    >
                      Status
                    </TableCell>
                    <TableCell
                      style={{ width: "30%" }}
                      className={classes.theadercell}
                      align="left"
                    >
                      Tags
                    </TableCell>
                    <TableCell className={classes.theadercell} align="right">
                      &nbsp;
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {rows.map((row, rindex) => (
                    <TableRow
                      className={
                        rindex % 2 === 0 ? classes.evenrow : classes.oddrow
                      }
                      key={rindex}
                    >
                      <TableCell
                        className={classes.headingColor}
                        component="th"
                        scope="row"
                      >
                        {row.question}
                      </TableCell>
                      <TableCell align="left">{row.createddate}</TableCell>
                      <TableCell align="left">{row.email}</TableCell>
                      <TableCell align="left">{row.status}</TableCell>
                      <TableCell align="left">
                        {[...row.tags].splice(0, 3).map((el, itag) => {
                          return (
                            <Chip
                              className={classes.chipColor}
                              label={el}
                              key={`${itag}-${rindex}`}
                            />
                          );
                        })}

                        {row.tags.length > 3 && (
                          <a className={classes.headingColor}>View all</a>
                        )}
                      </TableCell>
                      <TableCell align="right">
                        <Button
                          className={[classes.btnPrimary, classes.delBtn].join(' ')}
                          variant="contained"
                        >
                          DELETE
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}
