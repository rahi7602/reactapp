import React from "react";
import classes from "../../assets/global.scss";
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import clsx from "clsx";
import styles from "./questions.scss";
import Input from "@material-ui/core/Input";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Button from "@material-ui/core/Button";
import { Add, HelpOutline } from "@material-ui/icons";
import Grid from "@material-ui/core/Grid";



export class FormWizard extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state={
        name:'',
        questionselected:false,
        questions:[]
    };
    this.addQuestion = this.addQuestion.bind(this);
  }

  componentDidMount(){
     let questions = [];
     let question = {qtext:'',selected:false};
     for(let i=0;i<3;i++)
     {
       questions.push(question);
     }
     this.setState({questions:questions});
  }

  handleChange = name => event => {
    this.setState({ name: event.target.value });
   
  };

  submitForm =()=>{
      alert('form_submit');
  }
  addQuestion(){
  
    let prevQuestions = [...this.state.questions];
    let question = {qtext:'',selected:false};
    prevQuestions.push(question);
    this.setState({questions:prevQuestions});
  }

  gotoselectQuestion(){
    this.setState({questionselected:true});
  }

  handleQuestionchnage(e,k){
    let prevQuestions = [...this.state.questions];
    let question = {...prevQuestions[k]};
    question['qtext'] = e.target.value;
    prevQuestions[k] = question;
    this.setState({questions:prevQuestions});
  }

  selectQuestion(k){
    let prevQuestions = [...this.state.questions].map(q=>{
      q.selected = false;
      return q;
    });
    let question = {...prevQuestions[k]};
    question['selected'] = true;
    prevQuestions[k] = question;
    this.setState({questions:prevQuestions});
  }
//   FRISTSTEP START //  FRISTSTEP START
//   FRISTSTEP START //   FRISTSTEP START

  firstStep(activeClasss) {
      
    return (
      <form className={[styles.FromWrap,styles.questionWrap ].join(' ')}>
        <FormControl
          fullWidth
          className={[classes.margin, styles.field].join(" ")}
        >
           
          <InputLabel htmlFor="adornment-amount" className={styles.label}>
            Question Text
          </InputLabel>
          <TextField
            required
            multiline
            rows="4"      
            margin="normal"
            variant="outlined"
            name="qsTxt"
            className={[styles.textField].join(' ')}
            placeholder='This is a sample of the text that you can use as part of your question….'
          />
        </FormControl>
        {!this.state.questionselected && this.state.questions.map((q,k)=>{
            return (<FormControl key={k} fullWidth className={classes.margin}>
            <InputLabel htmlFor="adornment-amount" className={styles.label}>
              Option {k+1} Text
            </InputLabel>
            <TextField
              className={[styles.textField].join(' ')}
              margin="normal"
              value={q.qtext}
              variant="outlined"   
              onChange={(e)=>this.handleQuestionchnage(e,k)}        
              name="txt1"
              placeholder={`Text for option ${k+1}`}

            />
          </FormControl>)
          })}


          {this.state.questionselected && this.state.questions.map((q,k)=>{
            return (<FormControl onClick={()=>this.selectQuestion(k)} key={k} fullWidth className={classes.margin}>
            <InputLabel htmlFor="adornment-amount" className={styles.label}>
              Option {k+1} Text
            </InputLabel>
            <TextField
              className={q.selected?[styles.textField , styles.selected].join(' '):[styles.textField, styles.select].join(' ')}
              margin="normal"
              value={q.qtext}
              disabled={true}
              variant="outlined"       
              name="txt1"
              placeholder={`Text for option ${k+1}`}

            />
          </FormControl>)
          })}
      
        {/* <FormControl fullWidth className={classes.margin}>
          <InputLabel htmlFor="adornment-amount" className={styles.label}>
            Option 2 Text
          </InputLabel>
          <TextField
       
            className={[styles.textField , styles.selected].join(' ')}
            defaultValue=" "
            margin="normal"
       
            variant="outlined"
            defaultValue=''
            placeholder='Text for option 2'
          />
        </FormControl>
        <FormControl fullWidth className={classes.margin}>
          <InputLabel htmlFor="adornment-amount" className={styles.label}>
            Option 3 Text
          </InputLabel>
          <TextField
            id="outlined-bare"
            className={[styles.textField , styles.select].join(' ')}
            defaultValue=" " 
            margin="normal"
            variant="outlined"
            name="txt3"
            placeholder='Text for option 3'
          />
        </FormControl> */}
        <Button
          size="small"
          onClick={this.addQuestion}
          className={[classes.margin, styles.addBtn].join(" ")}>
          <Add style={{ verticalAlign: "sub" }} /> ADD
        </Button>
        <Grid item xs={12}>

         {!this.state.questionselected?(  <Button variant="contained" type='button' onClick={()=>this.gotoselectQuestion()} component="span" className={[classes.margin, styles.submitBtn].join(" ")}>
          SELECT CORRECT ANSWERS
        </Button>):(  <Button variant="contained" type='button'component="span" className={[classes.margin, styles.submitBtn].join(" ")}>
           FINISH
        </Button>)}
      
        </Grid>
      </form>
    );
  }


//   SECONDSTEP START //  SECONDSTEP START
//   SECONDSTEP START //   SECONDSTEP START
  SecondStep() {
    return (
    <form className={[styles.FromWrap,styles.questionWrap ].join(' ')}>
     <FormControl
          fullWidth
          className={[classes.margin, styles.field].join(" ")}
        >
          <InputLabel htmlFor="adornment-amount" className={styles.label}>
            Question Text
          </InputLabel>
          <TextField
            required
            id="outlined-multiline-static"
            multiline
            rows="12"
            defaultValue=" "
            onChange={this.handleChange()}            
            className={classes.textField}
            margin="normal"
            variant="outlined"
            name="qsTxt"
            defaultValue=''
            placeholder='This is a sample of the text that you can use as part of your question….'
          />
        </FormControl>
        <Button variant="contained" type='submit' onClick={this.submitForm} component="span" className={[classes.margin, styles.submitBtn ,styles.finish].join(" ")}>
        FINISH
        </Button>
    </form>
    );
  }


  //   THIRDSTEP START //  THIRDSTEP START
//   THIRDSTEP START //   THIRDSTEP START

  ThirdStep = () => {
      return (
        <form className={[styles.FromWrap,styles.questionWrap ].join(' ')}>
         <FormControl fullWidth className={classes.margin}>
          <InputLabel htmlFor="adornment-amount" className={styles.label}>
          Question Text
          </InputLabel>
          <TextField
            id="outlined-bare"
            className={styles.textField}
            defaultValue=" "
            margin="normal"
            onChange={this.handleChange()}   
            variant="outlined"
            inputProps={{ "aria-label": "bare" }}            
            name="txt1"
            defaultValue=''
            placeholder='This is a sample of the text that you can use as part of your question….'
          />
       </FormControl>
        <FormControl fullWidth className={[classes.margin,styles.FormGroup].join(' ')}>
          <InputLabel htmlFor="adornment-amount" className={styles.label}>
            Option 1 Text
          </InputLabel>
          <TextField
            id="outlined-bare"
            className={styles.textField}
            defaultValue=" "
            margin="normal"
            onChange={this.handleChange()}   
            variant="outlined"
            inputProps={{ "aria-label": "bare" }}            
            name="txt1"
            defaultValue=''
            placeholder='Text for option 1'
          />
           <InputLabel htmlFor="adornment-amount" className={styles.label}>
           Match for Option 1 Text
          </InputLabel>
          <TextField
            id="outlined-bare"
            className={styles.textField}
            defaultValue=" "
            margin="normal"
            onChange={this.handleChange()}   
            variant="outlined"
            inputProps={{ "aria-label": "bare" }}            
            name="txt1"
            defaultValue=''
            placeholder='Match for Option 1 Text'
          />
        </FormControl>
        <FormControl fullWidth className={[classes.margin,styles.FormGroup].join(' ')}>
          <InputLabel htmlFor="adornment-amount" className={styles.label}>
            Option 2 Text
          </InputLabel>
          <TextField
            id="outlined-bare"
            className={styles.textField}
            defaultValue=" "
            margin="normal"
            onChange={this.handleChange()}   
            variant="outlined"
            inputProps={{ "aria-label": "bare" }}
            name="txt2"
            defaultValue=''
            placeholder='Text for option 2'
          />
          <InputLabel htmlFor="adornment-amount" className={styles.label}>
           Match for Option 2 Text
          </InputLabel>
          <TextField
            id="outlined-bare"
            className={styles.textField}
            defaultValue=" "
            margin="normal"
            onChange={this.handleChange()}   
            variant="outlined"
            inputProps={{ "aria-label": "bare" }}            
            name="txt1"
            defaultValue=''
            placeholder='Match for Option 2 Text'
          />
        </FormControl>
        <FormControl fullWidth className={[classes.margin,styles.FormGroup].join(' ')}>
          <InputLabel htmlFor="adornment-amount" className={styles.label}>
            Option 3 Text
          </InputLabel>
          <TextField
            id="outlined-bare"
            className={styles.textField }
            defaultValue=" "
            onChange={this.handleChange()}   
            margin="normal"
            variant="outlined"
            inputProps={{ "aria-label": "bare" }}
            name="txt3"
            defaultValue=''
            placeholder='Text for option 1'
          />
           <InputLabel htmlFor="adornment-amount" className={styles.label}>
           Match for Option 3 Text
          </InputLabel>
          <TextField
            id="outlined-bare"
            className={styles.textField}
            defaultValue=" "
            margin="normal"
            onChange={this.handleChange()}   
            variant="outlined"
            inputProps={{ "aria-label": "bare" }}            
            name="txt1"
            defaultValue=''
            placeholder='Match for Option 3 Text'
          />
        </FormControl>
        <Button
          size="small"
          className={[classes.margin, styles.addBtn].join(" ")}>
          <Add style={{ verticalAlign: "sub" }} /> ADD
        </Button>
        <Grid item xs={12}>
        <Button variant="contained" type='submit' onClick={this.submitForm} component="span" className={[classes.margin, styles.submitBtn].join(" ")}>
        FINISH
        </Button>
        </Grid>
      </form>
      )
  }


  //   FOURTHSTEP START //  FOURTHSTEP START
//   FOURTHSTEP START //   FOURTHSTEP START

  FourthStep = () => {
    return (
      <form className={[styles.FromWrap,styles.questionWrap ].join(' ')}>
       <FormControl fullWidth className={classes.margin}>
        <InputLabel htmlFor="adornment-amount" className={styles.label}>
        Question Text
        </InputLabel>
        <TextField
          id="outlined-bare"
          className={styles.textField}
          defaultValue=" "
          margin="normal"
          onChange={this.handleChange()}   
          variant="outlined"
          inputProps={{ "aria-label": "bare" }}            
          name="txt1"
          defaultValue=''
          placeholder='This is a sample of the text that you can use as part of your question….'
        />
     </FormControl>
      <FormControl fullWidth className={[classes.margin , styles.FormGroup2 ].join(' ')}>
        <InputLabel htmlFor="adornment-amount" className={styles.label}>
          Option 1 Text
        </InputLabel>
        <TextField
          id="outlined-bare"
          className={styles.textField}
          defaultValue=" "
          margin="normal"
          onChange={this.handleChange()}   
          variant="outlined"
          inputProps={{ "aria-label": "bare" }}            
          name="txt1"
          defaultValue=''
          placeholder='Text for option 1'
        />
        
      </FormControl>
      <FormControl fullWidth className={[classes.margin , styles.FormGroup2 ].join(' ')}>
        <InputLabel htmlFor="adornment-amount" className={styles.label}>
          Option 2 Text
        </InputLabel>
        <TextField
          id="outlined-bare"
          className={styles.textField}
          defaultValue=" "
          margin="normal"
          onChange={this.handleChange()}   
          variant="outlined"
          inputProps={{ "aria-label": "bare" }}
          name="txt2"
          defaultValue=''
          placeholder='Text for option 2'
        />
       
      </FormControl>
      <FormControl fullWidth className={[classes.margin , styles.FormGroup2 ].join(' ')}>
        <InputLabel htmlFor="adornment-amount" className={styles.label}>
          Option 3 Text
        </InputLabel>
        <TextField
          id="outlined-bare"
          className={styles.textField }
          defaultValue=" "
          onChange={this.handleChange()}   
          margin="normal"
          variant="outlined"
          inputProps={{ "aria-label": "bare" }}
          name="txt3"
          defaultValue=''
          placeholder='Text for option 1'
        />
        
      </FormControl>
      <p>Please move the options into the correct order before finishing.</p>
      <Button
        size="small"
        className={[classes.margin, styles.addBtn].join(" ")}>
        <Add style={{ verticalAlign: "sub" }} /> ADD
      </Button>

      <p style={{fontSize:'13px',color:'#5DA2D5'}}>Please move the options into the correct order before finishing.</p>
      <Grid item xs={12}>
      <Button variant="contained" type='submit' onClick={this.submitForm} component="span" className={[classes.margin, styles.submitBtn].join(" ")}>
      FINISH
      </Button>
      </Grid>
    </form>
    )
}

  render() {
    return (
      <React.Fragment>
        {this.props.formIdentity === "Multiple Choice" && this.firstStep('active')}
        {this.props.formIdentity === "Text" && this.SecondStep()}
        {this.props.formIdentity === "Matching" && this.ThirdStep()}
        {this.props.formIdentity === "Sorting" && this.FourthStep()}
      </React.Fragment>
    );
  }
}
