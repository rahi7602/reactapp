import React from 'react'
import classes from './login.scss'
import PropTypes from 'prop-types'
import {Route, Link} from 'react-router-dom'
import styled from 'styled-components'

export class LoginPage extends React.Component{
    render(){
        return(
            <div className={classes.main}>
                <div className={classes.header}>
                    <Link to={"/"}><img className={classes.img_size} src={require("../../assets/imgs/dashboard/professor_mark.png")}/></Link>
                    <ul className={classes.ul_style}>
                        <li><a>WHAT DOES THIS DO?</a></li>
                        <li><a>WHAT IS IT FOR?</a></li>
                        <li><a>HOW MUCH DOES IT COST?</a></li>
                        <li><a>WHO CAN I TALK TO?</a></li>
                        <li><Link to={"/login"} className={classes.signin}>Sign In</Link></li>
                    </ul>
                </div>

                <div className={classes.content}>
                    <div className={classes.form_left}>
                        <img className={classes.img_style} src={require('../../assets/imgs/dashboard/login_bg.png')} />
                        <form className={classes.login_form}>
                            <p className={classes.sign_title}>Sign In</p>
                            <p><input className={classes.input_item} type="text" placeholder="User Name" /></p>
                            <p><input className={classes.input_item} type="password" placeholder="Password" /></p>
                            <p><Link to={'/main/dashboard'} className={classes.sign_btn}>SIGN IN</Link></p>
                        </form>
                    </div>
                    <div className={classes.form_right}>
                        <p className={classes.red_text}> Let's Go!</p>
                        <p className={classes.under_text}>
                            Sign in to your account to access all your ProfessorDesk materials in the cloud! Everything you need is always at your fingertips!
                        </p>
                    
                    </div>

                    
                </div>

            </div>

        )
    }
}