import React from 'react'
import classes from '../../assets/global.scss'
import PropTypes from 'prop-types'
import { Route, Link } from 'react-router-dom'
import styled from 'styled-components'
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import Paper from '@material-ui/core/Paper';
import { Dialog, Button, DialogTitle, DialogContent,Chip, Typography, DialogActions } from '@material-ui/core';
import {Add,HelpOutline} from '@material-ui/icons'
import {FaQuestion} from 'react-icons/fa'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import * as faker from 'faker';
import styles from "./questions-set.scss";
import setQues from './../../assets/imgs/main/setQues.png';
import addquWht from './../../assets/imgs/main/addquWht.png';
import questionSetGrey from './../../assets/imgs/main/questionSetGrey.png'
import Input from "@material-ui/core/Input";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Tooltip from "@material-ui/core/Tooltip";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

function createData(question, createddate, email, status, tags) {
    
    return {question, createddate, email, status, tags };
  }
  
  const rows = [];

  for(let i=0;i<=40;i++){
      let tags = faker.random.words().split(" ");
      let row =  createData(faker.random.words(),'2015-01-01',faker.internet.email(),
      faker.random.arrayElement(['Draft','Published','Sent']), tags);
      rows.push(row);
  }
  const currencies = [
    {
      value: "Date Created",
      label: "Date Created"
    },
    {
      value: "teacher",
      label: "Teacher"
    },
    {
      value: "Parents",
      label: "parents"
    }
  ];




export class QuestionsSetPage extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);

        this.state = {
            open: false,
            role:currencies[0]
        };
    }

    handleClose() {
        this.setState({ open: false });
    }

    handleShow() {
        this.setState({ open: true });
    }

    handleRole = (e) => {
        console.log("...role",e);
       this.setState({'role': e.target });
    };

    renderModal() {
        return (
          <div>
            <Dialog
              onClose={this.handleClose}
              aria-labelledby="form-dialog-title"
              open={this.state.open}
              fullWidth={true}
              scroll={"body"}
              maxWidth={"sm"}
              className={[
                styles.DialogContainer,
                styles.setDialog
              ].join(" ")}
            >
              <DialogTitle
                id="form-dialog-title"
                className={[
                  classes.DialogHeading,
                  styles.userDialog
                ].join(" ")}
              >
                <img
                  src={questionSetGrey}
                  style={{
                    width: "32px",
                    verticalAlign: "top",
                    marginRight: "16px"
                  }}
                />
                Add New Question Set
              </DialogTitle>
              <DialogContent dividers>
                <form>
                  <Grid item xs={6}>
                    <FormControl
                      fullWidth
                      className={[
                        classes.margin,
                        styles.FormGroup2,
                        styles.FormGroup3
                      ].join(" ")}
                    >
                      <InputLabel
                        htmlFor="adornment-amount"
                        className={styles.label}
                      >
                        Name
                      </InputLabel>
                      <TextField
                        id="outlined-bare2"
                        className={styles.textField}
                        margin="normal"
                        variant="outlined"
                        inputProps={{ "aria-label": "bare" }}
                        name="txt1"
                        defaultValue=""
                        placeholder="Text for option 1"
                      />
                    </FormControl>
                  </Grid>
                  <Grid container spacing={1}>
                    <Grid item xs={3}>
                      <Typography
                        className={[
                          classes.headingColor,
                          classes.pageTitle,
                          styles.formTag1
                        ].join(" ")}
                        variant="inherit"
                      >
                        Select Questions
                      </Typography>
                      <Typography
                        className={[
                          classes.headingColor,
                          classes.pageTitle,
                          styles.formTag
                        ].join(" ")}
                        variant="inherit"
                      >
                        Filters:
                      </Typography>
                    </Grid>
                    <Grid item xs={3}>
                    
                    <FormControl fullWidth className={[classes.margin,styles.setSelect].join(' ')}>
                   
                      <TextField
                        id="filled-select-currency"
                        select
                        label="With"
                        className={[
                          classes.textField,
                          styles.selectSet
                        ].join(" ")}
                        value={this.state.role.value}
                        onChange={this.handleRole}
                        SelectProps={{
                          MenuProps: {
                            className: classes.menu
                          }
                        }}
                        margin="normal"
                        variant="outlined"
                      >
                        {currencies.map(option => (
                          <MenuItem
                            key={option.value}
                            value={option.value}
                          >
                            {option.label}
                          </MenuItem>
                        ))}
                      </TextField>
                      </FormControl>
                    </Grid>
                    <Grid item xs={3}>
                    <FormControl fullWidth className={[classes.margin,styles.setSelect].join(' ')}>
                   
                   <TextField
                     id="filled-select-currency"
                     select
                     label="With"
                     className={[
                       classes.textField,
                       styles.selectSet
                     ].join(" ")}
                     value={this.state.role.value}
                     onChange={this.handleRole}
                     SelectProps={{
                       MenuProps: {
                         className: classes.menu
                       }
                     }}
                     margin="normal"
                     variant="outlined"
                   >
                     {currencies.map(option => (
                       <MenuItem
                         key={option.value}
                         value={option.value}
                       >
                         {option.label}
                       </MenuItem>
                     ))}
                   </TextField>
                   </FormControl>
                    </Grid>
                    <Grid item xs={3}>
                    <FormControl fullWidth className={[classes.margin,styles.setSelect].join(' ')}>
                   
                   <TextField
                     id="filled-select-currency"
                     select
                     label="With"
                     className={[
                       classes.textField,
                       styles.selectSet
                     ].join(" ")}
                     value={this.state.role.value}
                     onChange={this.handleRole}
                     SelectProps={{
                       MenuProps: {
                         className: classes.menu
                       }
                     }}
                     margin="normal"
                     variant="outlined"
                   >
                     {currencies.map(option => (
                       <MenuItem
                         key={option.value}
                         value={option.value}
                       >
                         {option.label}
                       </MenuItem>
                     ))}
                   </TextField>
                   </FormControl>
                    </Grid>
                  </Grid>
                
             

                <Grid container spacing={1}>
                <Paper style={{width:'100%'}} className={styles.filterTable} >
                <Table >
                    <TableHead className={styles.TableHead}>
                    <TableRow >
                        <TableCell align="center" style={{width:'30%'}} className={classes.theadercell}>Question Text</TableCell>
                        <TableCell style={{width:'35%'}} className={classes.theadercell} align="left">Date Created</TableCell>
                        <TableCell style={{width:'25%'}} className={classes.theadercell} align="left">Created By</TableCell>                        
                        {/* <TableCell style={{width:'30%'}} className={classes.theadercell} align="left">Tags</TableCell> */}
                        
                    </TableRow>
                    </TableHead>
                        <TableBody>
                        {rows.map((row,rindex) => (
                        <TableRow className={rindex%2===0?styles.evenrow:styles.oddrow} key={rindex}>
                        <TableCell className={classes.headingColor} component="th" scope="row">
                          
                            <FormControlLabel
                    control={
                      <Checkbox
                        // checked={}
                        // onChange={}
                        value={row.question}
                        color="primary"
                      />
                    }
                    className={styles.checkLabel}
                    label={row.question}
                    key={rindex}
                  />
                        </TableCell>
                        <TableCell align="left">{row.createddate}</TableCell>
                        <TableCell align="left">{row.email}</TableCell>                       
                        {/* <TableCell align="left">
                            {[...row.tags].splice(0,3).map((el,itag)=>{
                                return <Chip className={classes.chipColor} label={el} key={`${itag}-${rindex}`} />
                            })}

                            {row.tags.length>3 && (<a className={classes.headingColor}>View all</a>)}
                        </TableCell> */}
                       
                        </TableRow>
                    ))}
                        </TableBody>
                    </Table>
                 </Paper>
                </Grid>

                <Button
                  variant="contained"
                  type="submit"
                  onClick={this.submitForm}
                  component="span"
                  className={[classes.margin, styles.submitBtn, styles.setQBtn].join(" ")}
                  style={{marginTop:'20px'}}
                >
                  CREATE SET
                </Button>
                </form>
              </DialogContent>
            
            </Dialog>
          </div>
        );
    }

    render() {

        return ( <div className={classes.root}>
            {this.renderModal()}
            <Grid container spacing={3}>
              <Grid item xs={6}>          
            <Typography
              className={[classes.headingColor, classes.pageTitle].join(' ')}
              variant="h5"
              gutterBottom
            >
              <img src={setQues} className={classes.imgStyle} />
                Manage Question Sets
                </Typography>
                
              </Grid>
             
              <Grid style={{textAlign:"right"}} item xs={6}>
                <Button onClick={this.handleShow} className={classes.btnPrimary} variant="contained" >
                <img src={addquWht} style={{width:'30px',marginRight:'12px'}} />ADD QUESTION SET
                </Button>
              </Grid>
            </Grid>
            <Grid container spacing={3}>
              <Grid item xs={12}>
              <h2 className={classes.pageHeading}> All Question Sets</h2>
              </Grid>
              <Grid item  xs={12}>
              <Paper >
                <Table >
                    <TableHead className={classes.TableHead}>
                    <TableRow >
                        <TableCell style={{width:'20%'}} className={classes.theadercell}>Question Set Name</TableCell>
                        <TableCell style={{width:'15%'}} className={classes.theadercell} align="left">Date Created</TableCell>
                        <TableCell style={{width:'20%'}} className={classes.theadercell} align="left">Created By</TableCell>
                        <TableCell style={{width:'10%'}} className={classes.theadercell} align="left">Status</TableCell>
                        <TableCell style={{width:'30%'}} className={classes.theadercell} align="left">Tags</TableCell>
                        <TableCell className={classes.theadercell} align="right">&nbsp;</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {rows.map((row,rindex) => (
                        <TableRow className={rindex%2===0?classes.evenrow:classes.oddrow} key={rindex}>
                        <TableCell className={classes.headingColor} component="th" scope="row">
                            {row.question}
                        </TableCell>
                        <TableCell align="left">{row.createddate}</TableCell>
                        <TableCell align="left">{row.email}</TableCell>
                        <TableCell align="left">{row.status}</TableCell>
                        <TableCell align="left">
                            {[...row.tags].splice(0,3).map((el,itag)=>{
                                return <Chip className={classes.chipColor} label={el} key={`${itag}-${rindex}`} />
                            })}

                            {row.tags.length>3 && (<a className={classes.headingColor}>View all</a>)}
                        </TableCell>
                        <TableCell align="right">
                        <Button
                          className={[classes.btnPrimary, classes.delBtn].join(' ')}
                          variant="contained"
                        >
                          DELETE
                        </Button>
                        </TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
                </Paper>
              </Grid>
            
            </Grid>
          </div>)
    }

}