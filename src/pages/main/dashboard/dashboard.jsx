import React from 'react'
import classes from './dashboard.scss'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'

class DashBoardPage extends React.Component{

    handleRoute = routePath =>{
        this.props.history.push(`/main/${routePath}`)
    }

    render(){
        return(
            <div className={classes.main}>
                <h2><img src={require('../../../assets/imgs/main/ic_dash.png')}/>Dashboard</h2>
                <div className = {classes.container}>
                    <div onClick={()=>this.handleRoute('users')}  className={classes['container-item']}>
                        <img src={require('../../../assets/imgs/main/img_dash_user.png')}/>
                        <p>Manage Users</p>
                    </div>
                    <div onClick={()=>this.handleRoute('roles')}  className={classes['container-item']}>
                        <img src={require('../../../assets/imgs/main/img_dash_role.png')}/>
                        <p>Manage Roles</p>
                    </div>
                    <div onClick={()=>this.handleRoute('questions')}  className={classes['container-item']}>
                        <img src={require('../../../assets/imgs/main/img_dash_question.png')}/>
                        <p>Manage Questions</p>
                    </div>
                    <div onClick={()=>this.handleRoute('questions-set')}  className={classes['container-item']}>
                        <img src={require('../../../assets/imgs/main/img_dash_qs.png')}/>
                        <p>Manage Question Sets</p>
                    </div>
                </div>
            </div>
        )
    }
}

let DashBoardWithrouter = withRouter(DashBoardPage);
export  {DashBoardWithrouter as DashBoard}