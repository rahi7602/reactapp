import React from 'react'
import classes from './main-layout.scss'
import PropTypes from 'prop-types'
import {Route, Link,withRouter} from 'react-router-dom'
import styled from 'styled-components'
import { HeaderMenu } from '../../components/main-menu';
import { SideBar } from '../../components/side-bar';

import { DashBoard } from '../../pages/main/dashboard';
import {QuestionsPage} from '../../pages/questions'
import {UsersPage} from '../../pages/users'
import {RolesPage} from '../../pages/roles'
import {QuestionsSetPage} from '../../pages/questions-set'

export class MainLayout extends React.Component{

    constructor(props){
        super(props);
        this.state = {componemtpath:'dashboard'};
        this.routeChanged = this.routeChanged.bind(this);
    }

    componentDidUpdate(prevProps) {
        if (this.props.location !== prevProps.location) {
            this.routeChanged();
        }
      }

    componentDidMount(){

      this.routeChanged();
        
    } 
    routeChanged(){
        let  componemtpath = this.props.match.params.componemtpath || 'dashboard';
        this.setState({componemtpath:componemtpath});
    }

    render(){
        const {componemtpath} = this.state;
        return(
            <div className={classes.main}>
                <HeaderMenu/>
                <div className={classes['content']}>
                    <SideBar
                    {...this.props}
                    />
                   
                   {componemtpath==='dashboard' && <DashBoard/>}
                   {componemtpath==='users' && <UsersPage/>}
                   {componemtpath==='roles' && <RolesPage/>}
                   {componemtpath==='questions' && <QuestionsPage/>}
                   {componemtpath==='questions-set' && <QuestionsSetPage/>}
                    
                </div>
            </div>
        )
    }
}

let MainLayoutWithrouter = withRouter(MainLayout);
export  {MainLayoutWithrouter as MainPage}