import React from 'react'
import PropTypes from 'prop-types'
import { FlexContainer } from '../'
import { constants } from '../../__data__'
import styles from './cg-tshirt.scss'
import { EditButton } from '../widgets/edit-button'
import { DesignButton } from '../widgets/design-button'
import ZoomIn from '@material-ui/icons/ZoomIn'
import ZoomOut from '@material-ui/icons/ZoomOut'
import { ColorPicker } from '../widgets/color-picker'
import { Canvas } from 'react-three-fiber'

import { Simple } from '../React3'

const { CG_LAYOUT_MODE, EDIT_BUTTON_SELECT, DESIGN_BUTTON_SELECT } = constants

export class CGTShirt extends React.PureComponent {
    state={
        edit_button_selected : EDIT_BUTTON_SELECT.DEFAULT,
        design_button_selected : DESIGN_BUTTON_SELECT.DEFAULT

    }
    onClickEditButton(item){
        this.setState({edit_button_selected: item})
        console.log(item)
    }
    onClickDesignButton(item){
        this.setState({design_button_selected: item})
    }

    render () {
        const { mode } = this.props
        const mainContainerDirection = mode === CG_LAYOUT_MODE.ROW ? 'row' : 'column-reverse'
        const toolContainerDirection = mode === CG_LAYOUT_MODE.ROW ? 'row' : 'column-reverse'
        const editorToolsContainerDirection = mode === CG_LAYOUT_MODE.ROW ? 'column' : 'row'

        return (
            <FlexContainer direction={mainContainerDirection} className={styles['root-main']} >
                <FlexContainer direction={toolContainerDirection} className={styles['left-toolbar']}>
                    <FlexContainer direction={editorToolsContainerDirection} className={styles['editor-tools-container']}>
                        <EditButton isActive={(this.state.edit_button_selected==EDIT_BUTTON_SELECT.COLOR_PICKER)}
                         onClick={this.onClickEditButton.bind(this, EDIT_BUTTON_SELECT.COLOR_PICKER)}>
                            <div className={styles['color-picker']}>
                                <img src={require("../../assets/imgs/ic_threebox.png")}></img>
                                <p>Color Picker</p>
                            </div>
                        </EditButton>
                        <EditButton isActive={(this.state.edit_button_selected==EDIT_BUTTON_SELECT.ADD_TEXT)} 
                        onClick={this.onClickEditButton.bind(this, EDIT_BUTTON_SELECT.ADD_TEXT)}>
                            <div className={styles['add-text']}>
                                <label>Aa</label>
                                <span>Add Text</span>
                            </div>
                        </EditButton>
                        <EditButton isActive={(this.state.edit_button_selected==EDIT_BUTTON_SELECT.PERSONALIZE)} 
                        onClick={this.onClickEditButton.bind(this, EDIT_BUTTON_SELECT.PERSONALIZE)}>
                            <div className = {styles['add-name']}>
                                <label>NAMES 00</label>
                                <span>Personalize</span>
                            </div>
                        </EditButton>
                        <EditButton isActive={(this.state.edit_button_selected==EDIT_BUTTON_SELECT.ADD_IMAGE)} 
                        onClick={this.onClickEditButton.bind(this, EDIT_BUTTON_SELECT.ADD_IMAGE)}>
                            <div className = {styles['add-image']}>
                                <img src={require("../../assets/imgs/ic_addImg.png")}></img>
                                <span>Add Image</span>
                            </div>
                        </EditButton>
                    </FlexContainer>

                    <FlexContainer direction='column' className={styles['editor-controls-container']}>
                        <ColorPicker opened={(this.state.edit_button_selected==EDIT_BUTTON_SELECT.COLOR_PICKER)}/>
                    </FlexContainer>
                </FlexContainer>

                <FlexContainer direction='row' className={styles['preview-container']}>
                    <div className={styles['scene-container']}>
                        {/* <Simple/> */}
                        <Canvas>
                            <Simple verticles={[[-1, 0, 0], [0, 1, 0], [1, 0, 0], [0, -1, 0], [-1, 0, 0]]} color={"blue"}/>
                        </Canvas>
                        
                    </div>
                    <FlexContainer direction='column' className={styles['design-tools-container']}>
                        <div className={styles['design-button-group']}>
                            <DesignButton labelTxt="Front" src ={require('../../assets/imgs/ic_addImg.png')} onClick={this.onClickDesignButton.bind(this, DESIGN_BUTTON_SELECT.FRONT)}
                             isActive={(this.state.design_button_selected==DESIGN_BUTTON_SELECT.FRONT)} />
                            <DesignButton labelTxt="Back" src ={require('../../assets/imgs/ic_addImg.png')}  onClick={this.onClickDesignButton.bind(this, DESIGN_BUTTON_SELECT.BACK)}
                            isActive={(this.state.design_button_selected==DESIGN_BUTTON_SELECT.BACK)} />
                            <DesignButton labelTxt="R.Sleeve" src ={require('../../assets/imgs/ic_addImg.png')} onClick={this.onClickDesignButton.bind(this, DESIGN_BUTTON_SELECT.RSLEEVE)}
                             isActive={(this.state.design_button_selected==DESIGN_BUTTON_SELECT.RSLEEVE)} />
                            <DesignButton labelTxt="L.Sleeve" src ={require('../../assets/imgs/ic_addImg.png')}  onClick={this.onClickDesignButton.bind(this, DESIGN_BUTTON_SELECT.LSLLEEVE)}
                            isActive={(this.state.design_button_selected==DESIGN_BUTTON_SELECT.LSLLEEVE)} />
                        </div>
                        <div className={styles['zoom-button-group']}>
                            <EditButton isActive={false} className={styles['zoom-button']}>
                                <div >
                                    <ZoomIn className={styles.zoom}/>
                                </div>
                            </EditButton>
                            <EditButton isActive={false} className={styles['zoom-button']}>
                                <div>
                                    <ZoomOut className={styles.zoom}/>
                                </div>
                            </EditButton>
                        </div>
                    </FlexContainer>
                </FlexContainer>
            </FlexContainer>
        )
    }
}

CGTShirt.propTypes = {
    mode: PropTypes.oneOf([
        CG_LAYOUT_MODE.ROW,
        CG_LAYOUT_MODE.COLUMN,
    ])
}
