import React from 'react'
import classes from './side-bar.scss'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'

class SideBarPage extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            minHeight : 0
        }
    }

    componentDidMount(){
        console.log(this.props)
        this.setState({minHeight:this.props.height - 74})
    }

    handleRoute = routePath =>{
        this.props.history.push(`/main/${routePath}`)
    }

    render(){
        // let minHeight = this.state.minheight
        return (
            <div className={classes['main']} style={{minHeight: this.state.minHeight}}>
                <div onClick={()=>this.handleRoute('users')} className={classes['menu-item']}>
                    <img src={require('../../assets/imgs/main/img_user.png')}/>
                    <p>USERS</p>
                </div>
                <div onClick={()=>this.handleRoute('roles')} className={classes['menu-item']}>
                    <img src={require('../../assets/imgs/main/img_role.png')}/>
                    <p>ROLES</p>
                </div>
                <div onClick={()=>this.handleRoute('questions')} className={classes['menu-item']}>
                    <img src={require('../../assets/imgs/main/img_question.png')}/>
                    <p>QUESTIONS</p>
                </div>
                <div onClick={()=>this.handleRoute('questions-set')}  className={classes['menu-item']}>
                    <img src={require('../../assets/imgs/main/img_qs.png')}/>
                    <p>QUESTION SETS</p>
                </div>
            </div>
        )
    }
}
let sidebarWithrouter = withRouter(SideBarPage);
export  {sidebarWithrouter as SideBar}