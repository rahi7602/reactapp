import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

import styles from './flex-container.scss'

export const FlexContainer = ({ children, className, direction = 'row' }) => {
    const classNames = classnames(styles[`flex-${direction}`], className)

    return (
        <div className={classNames} >{children}</div>
    )
}

FlexContainer.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]),
    className: PropTypes.string,
    direction: PropTypes.oneOf([
        'row',
        'row-reverse',
        'column',
        'column-reverse',
    ])
}
