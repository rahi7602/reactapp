import React from 'react'
import classes from './menu.scss'
import PropTypes from 'prop-types'
import {Route, Link} from 'react-router-dom'
import styled from 'styled-components'

export class HeaderMenu extends React.Component{
    render(){
        return(
            <div className={classes['menu']}>
                <Link className={classes['professor']} to={'/main/dashboard'}>
                    <img src={require("../../assets/imgs/dashboard/professor_mark.png")} />
                </Link>
                <span>
                    <label>LOGGED IN AS PAULO MARTINO</label>
                    <img className={classes.avarta} src={require('../../assets/imgs/avarta.png')} />
                    <Link to={'/logout'}>LOG OUT</Link>
                </span>
             </div>
        )
    }
}