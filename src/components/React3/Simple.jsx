import React from 'react'
import * as THREE from 'three'
import PropTypes from 'prop-types'

export const Simple = ({ verticles, color}) => {
    const width = window.innerWidth; // canvas width
    const height = window.innerHeight; 
    const cameraPosition = new THREE.Vector3(0, 0, 5)
    const cubeRotation = new THREE.Euler()
    return (
        <group ref={ref => console.log('we have access to the instance')}>
        <line>
          <geometry
            attach="geometry"
            vertices={verticles.map(v => new THREE.Vector3(...v))}
            onUpdate={self => (self.verticesNeedUpdate = true)}
          />
          <lineBasicMaterial attach="material" color="black" />
        </line>
        <mesh 
          onClick={e => console.log('click')} 
          onPointerOver={e => console.log('hover')} 
          onPointerOut={e => console.log('unhover')}>
          <octahedronGeometry attach="geometry" />
          <meshBasicMaterial attach="material" color={color} opacity={0.5} transparent />
        </mesh>

       
      </group>
   
    )
}

Simple.propTypes = {
    verticles: PropTypes.array,
    color: PropTypes.string 
    
}



