import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import styles from './tool-button.scss'

export const ToolButton = ({ isActive, children, onClick, className}) => {

    var classNames = classnames(styles[`tool-button`], className)
    if(isActive){
        classNames = classnames(styles[`tool-button`], className, 
        styles['active-button'])
    }
    return (
        <button className={classNames} onClick={onClick} >{children}</button>
    )
}

ToolButton.propTypes = {
    isActive: PropTypes.bool,
    children: PropTypes.oneOfType([ 
        PropTypes.arrayOf(PropTypes.node), 
        PropTypes.node ]),
    className: PropTypes.string,
    onClick: PropTypes.func
}
