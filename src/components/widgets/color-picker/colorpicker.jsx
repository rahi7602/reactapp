
import React from 'react'
import classes from './colorpicker.scss'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import  {colorData} from './colorData'

const ColoredButtonItem = styled.button`
    width : 48px;
    height : 48px;
    background-color : ${({materialColor})=>materialColor};
    margin : 10px;
    border : "none";
    padding : 0;
    border-radius : 5px;
    cursor : pointer;
`

export class ColorPicker extends React.Component{

    state = {
        selColor : ""
    };

    constructor(props){
        super(props)
        this.setColor = this.setColor.bind(this)

    }

    setColor(color){
        console.log("color : ",color)
        this.setState({selColor: color})
    }
    onTypeColor(e){
        console.log('event : ',e.target.value)
        this.setColor(e.target.value)
    }
    render(){
        const {opened} = this.props
        var marginValue = 0
        let selectedColor = {backgroundColor : this.state.selColor}
        if(!opened){
            marginValue=-450
        }
        return(
            <div className={classes.rootMain} style={{marginLeft : marginValue}}>
                <div className={classes.header}>
                    <span>Choose Your Color</span>
                    <div className = {classes.roundDark} style={selectedColor}></div>
                    <img className = {classes.exitBtn} src={require("../../../assets/imgs/ic_exit.png")} />
                </div>
                <div className = {classes.content}>
                    <div className = {classes.contentHeader}>
                        <span>Current Color : </span>
                        <input className={classes.inputColor} 
                            value = {this.state.selColor}
                            onChange = {this.onTypeColor.bind(this)}
                        />
                        <button className = {classes.btnSetColor}>Set Color</button>
                    </div>
                    <div className = {classes.colorsContent}>
                        <button className={classes.firstColorItem}>
                            <img src = {require("../../../assets/imgs/ic_colorBtn.png")} width="100%" height= "100%"/>
                        </button>
                        {
                            colorData.map((item)=>(
                                <ColoredButtonItem key={item} materialColor={item} onClick={this.setColor.bind(this, item)}/>
                            ))
                        }
                    </div>
                </div>
            </div>
        );
    }


    
}

ColorPicker.propTypes = {
    opened: PropTypes.bool, 
    
}
