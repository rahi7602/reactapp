import React from 'react'
import PropTypes from 'prop-types'
import styles from './edit-button.scss'
import {ToolButton} from '../tool-button'
import classnames from 'classnames'

export const EditButton = ({ isActive, children, onClick, className}) => {

    const classNames = classnames(styles[`edit-button`], className)

    return (
        <ToolButton isActive={isActive} className={classNames} onClick={onClick} >{children}</ToolButton>
    )
}

EditButton.propTypes = {
    isActive: PropTypes.bool,
    children: PropTypes.oneOfType([ 
        PropTypes.arrayOf(PropTypes.node), 
        PropTypes.node ]),
    className: PropTypes.string,
    onClick: PropTypes.func
}
