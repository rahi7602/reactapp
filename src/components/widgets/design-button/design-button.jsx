import React from 'react'
import PropTypes from 'prop-types'
import styles from './design-button.scss'
import {ToolButton} from '../tool-button'

export const DesignButton = ({ src, labelTxt, isActive, onClick}) => {
    return (
        <ToolButton isActive={isActive} className={styles['design-button']} onClick={onClick} >
            <p>{labelTxt}</p>
            <img src={src}/>
        </ToolButton>
    )
}

DesignButton.propTypes = {
    src: PropTypes.string.isRequired, 
    labelTxt: PropTypes.string.isRequired, 
    isActive: PropTypes.bool, 
    onClick: PropTypes.func
}
