import React from 'react'
import { createMuiTheme,MuiThemeProvider } from '@material-ui/core/styles';
import { CGTShirt } from './components'
import { constants } from './__data__'
import {BrowserRouter, Switch, Route} from 'react-router-dom'
//pages
import {DemoPage} from './pages/demo'
import { LoginPage } from './pages/login';
import { MainPage } from './layouts/main-layout';
import {globalStyle} from './assets/global.scss';
const { CG_LAYOUT_MODE } = constants

const theme = createMuiTheme({

    palette: {
      default: {
        main: '#778899'
      },
      primary: {
        main: '#337ab7'
      },
      secondary: {
        main: '#778899'
      },
      companyBlue: { // doesnt work - defaults to a grey button
        main: '#65CFE9',
        contrastText: '#fff',
      },
      companyRed: { // doesnt work - grey button
          main: '#E44D69',
          contrastText: '#000',
      },
      contrastThreshold: 3,
      tonalOffset: 0.2,
    },
    typography: {
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
    },
    overrides: {
     

    
    }
  
  });

export class App extends React.PureComponent {
    constructor (props) {
        super(props)
        this.state = {
            mode: CG_LAYOUT_MODE.ROW,
            width: window.innerWidth,
            height: window.innerHeight
        }
    }

    updateDimensions() {
        this.setState({height: window.innerHeight, width : window.innerWidth})
        console.log(this.state.width, this.state.height)

        if(this.state.width>800){
            this.setState({mode : CG_LAYOUT_MODE.ROW})
        }else{
            this.setState({mode : CG_LAYOUT_MODE.COLUMN})
        }
    }
    //add event listener
    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
      }
    
      
      /**
       * Remove event listener
       **/
    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }

    onModeChanged = (e) => {
        this.setState({ mode: e.currentTarget.value })
    }

    render () {
        const { mode } = this.state
        return (
            <MuiThemeProvider theme={theme}>
            <BrowserRouter>

            <div style={{height: this.state.height, width: this.state.width}}>
                <Route
                    exact
                    path="/"
                    render={
                        (routeProps) => (
                            <DemoPage
                            {...routeProps}
                            />
                        )
                    }
                />
                <Route
                    path="/logout"
                    render={
                        (routeProps) => (
                            <DemoPage
                            {...routeProps}
                            />
                        )
                    }
                />
                <Route
                    path='/login'
                    // component = {LoginPage}
                    render={
                        (routeProps) => (
                            <LoginPage
                            {...routeProps}
                            />
                        )
                    }
                />
                <Route
                    path='/main/:componemtpath?'
                    // component = {LoginPage}
                    render={
                        (routeProps) => (
                            <MainPage
                            {...routeProps}
                            {...this.state}
                            />
                        )
                    }
                />
             
               
            </div>
            </BrowserRouter>
            </MuiThemeProvider>

        )
    }
}
