export const CG_LAYOUT_MODE = {
    ROW: 'row',
    COLUMN: 'column',
}
export const EDIT_BUTTON_SELECT = {
    DEFAULT :-1,
    COLOR_PICKER : 0,
    ADD_TEXT : 1,
    PERSONALIZE : 2,
    ADD_IMAGE : 3
}
export const DESIGN_BUTTON_SELECT = {
    DEFAULT:-1,
    FRONT : 0,
    BACK : 1,
    RSLEEVE : 2,
    LSLLEEVE : 3
}
